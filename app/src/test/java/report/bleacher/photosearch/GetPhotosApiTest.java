package report.bleacher.photosearch;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import report.bleacher.photosearch.model.Photo;
import report.bleacher.photosearch.model.Photos;
import report.bleacher.photosearch.service.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;

/**
 * Created by minhhung on 2/27/2018.
 */

@Config(constants = BuildConfig.class, sdk = 21,
        manifest = "app/manifests/AndroidManifest.xml")
@RunWith(RobolectricGradleTestRunner.class)
public class GetPhotosApiTest {
    private report.bleacher.photosearch.model.Response response = new report.bleacher.photosearch.model.Response();
    private ApiService mockedApiInterface;
    private Call<report.bleacher.photosearch.model.Response> mockedCall;
    report.bleacher.photosearch.model.Response rs;

    @Before
    public void setUp() throws Exception {
        mockedApiInterface = mock(ApiService.class);
        mockedCall = mock(Call.class);
        response.setPhotos(new Photos());
        List<Photo> photoList = new ArrayList<Photo>();
        Photo newPhoto = new Photo();
        newPhoto.setUrlS("www.yahoo.com");
        photoList.add(newPhoto);
        response.getPhotos().setPhoto(photoList);
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void GetPhotosApiTest() {
        // given
        Mockito.when(mockedApiInterface.getPhotos(eq(new HashMap<String, String>()))).thenReturn(mockedCall);
        // when
        Call<report.bleacher.photosearch.model.Response> call = mockedApiInterface.getPhotos((new HashMap<String, String>()));

        Mockito.doAnswer(new Answer<report.bleacher.photosearch.model.Response>() {
            @Override
            public report.bleacher.photosearch.model.Response answer(InvocationOnMock invocation) throws Throwable {
                Callback<report.bleacher.photosearch.model.Response> callback = invocation.getArgumentAt(0, Callback.class);
                callback.onResponse(mockedCall, Response.success(response));
                //callback.onFailure(mockedCall, new IOException());
                return response;
            }
        }).when(mockedCall).enqueue(any(Callback.class));


        try {
            call.enqueue(new Callback<report.bleacher.photosearch.model.Response>() {

                @Override
                public void onResponse(Call<report.bleacher.photosearch.model.Response> call, Response<report.bleacher.photosearch.model.Response> response) {
                    if (response.isSuccessful()) {
                        rs = response.body();
                        assertTrue(rs!=null);

                    }
                }

                @Override
                public void onFailure(Call<report.bleacher.photosearch.model.Response> call, Throwable t) {
                    if (call.isCanceled()) {

                    }
                }
            });
            assertEquals(rs.getPhotos().getPhoto().get(0).getUrlS(),"www.yahoo.com");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

