package report.bleacher.photosearch.application;

import android.app.Application;

import report.bleacher.photosearch.service.Client;


/**
 * Created by minhhung on 1/26/2018.
 */

public class App extends Application {
    private Client client;
    private static App mInstance;

    @Override
    public void onCreate() {
        super.onCreate();

        mInstance = this;
        client = new Client(mInstance);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    public static synchronized App getInstance() {
        return mInstance;
    }

    public Client getClientInstance() {
        return this.client;
    }

    public static Client getClient() {
        return getInstance().getClientInstance();
    }
}