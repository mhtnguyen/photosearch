package report.bleacher.photosearch.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import report.bleacher.photosearch.R;
import report.bleacher.photosearch.activities.DetailsActivity;
import report.bleacher.photosearch.model.Photo;

/**
 * Created by minhhung on 2/24/2018.
 */

public class ImageAdapter extends BaseAdapter implements Filterable {

    private LayoutInflater layoutinflater;
    private List<Photo> photoList;
    private List<Photo> filteredPhotoList;

    private Context context;
    private PhotoFilter photoFilter;

    public ImageAdapter(Context context, List<Photo> photoList) {
        this.context = context;
        layoutinflater =(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.photoList=photoList;
        this.filteredPhotoList=photoList;
        getFilter();
    }

    @Override
    public int getCount() {
        return filteredPhotoList == null ? 0 : filteredPhotoList.size();
    }

    @Override
    public Object getItem(int position) {
        return filteredPhotoList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;
        final Photo photo = (Photo) getItem(position);

        if(convertView == null){
            viewHolder = new ViewHolder();
            convertView = layoutinflater.inflate(R.layout.row_layout, parent, false);
            viewHolder.title = (TextView)convertView.findViewById(R.id.text_view);
            viewHolder.image = (ImageView)convertView.findViewById(R.id.image_view);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder)convertView.getTag();
        }
        if(photo!=null) {
            viewHolder.title.setText(photo.getTitle());
            viewHolder.image.setScaleType(ImageView.ScaleType.CENTER);
            final View finalConvertView = convertView;
            viewHolder.image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, DetailsActivity.class);
                    intent.putExtra(DetailsActivity.TITLE_EXTRA, photo.getTitle());
                    intent.putExtra(DetailsActivity.IMAGE_EXTRA, photo.getUrlL());
                    context.startActivity(intent);
                }
            });
            Picasso.with(context).load(photo.getUrlS()).error(R.drawable.error).into(viewHolder.image);
        }

        return convertView;
    }

    public void addAll(List<Photo> newItems) {
        if (newItems != null) {
            photoList.addAll(newItems);
            notifyDataSetChanged();
        }
    }

    @Override
    public Filter getFilter() {
        if (photoFilter == null) {
            photoFilter = new PhotoFilter();
        }

        return photoFilter;
    }

    static class ViewHolder{
        TextView title;
        ImageView image;
    }

    private class PhotoFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults filterResults = new FilterResults();
            if (constraint!=null && constraint.length()>0) {
                List<Photo> tempList = new ArrayList<Photo>();

                // search content in friend list
                for (Photo photo : photoList) {
                    if (photo.getTitle().toLowerCase().contains(constraint.toString().toLowerCase())) {
                        tempList.add(photo);
                    }
                }

                filterResults.count = tempList.size();
                filterResults.values = tempList;
            } else {
                filterResults.count = photoList.size();
                filterResults.values = photoList;
            }

            return filterResults;
        }

        /**
         * Notify about filtered list to ui
         * @param constraint text
         * @param results filtered result
         */
        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredPhotoList = (ArrayList<Photo>) results.values;
            if(filteredPhotoList.size()==0){
                switchBacktoUnfilteredList();
//                new Handler(Looper.getMainLooper()).post(new Runnable() {
//                        @Override
//                        public void run() {
//                            AlertDialog alertDialog = new AlertDialog.Builder(context).create();
//                            alertDialog.setTitle("No Data");
//                            alertDialog.setMessage("No photos returned");
//                            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
//                                    new DialogInterface.OnClickListener() {
//                                        public void onClick(DialogInterface dialog, int which) {
//                                            dialog.dismiss();
//                                        }
//                                    });
//                            alertDialog.show();
//                        }
//                    });

            }
            notifyDataSetChanged();
        }
    }

    public void switchBacktoUnfilteredList(){
        filteredPhotoList=photoList;
    }

}