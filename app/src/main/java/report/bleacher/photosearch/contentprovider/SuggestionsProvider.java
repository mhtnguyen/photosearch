package report.bleacher.photosearch.contentprovider;

import android.content.SearchRecentSuggestionsProvider;

/**
 * Created by minhhung on 2/25/2018.
 */

public class SuggestionsProvider extends SearchRecentSuggestionsProvider {
    public final static String AUTHORITY = "report.bleacher.photosearch.SuggestionsProvider";
    public final static int MODE = DATABASE_MODE_QUERIES;

    public SuggestionsProvider() {
        setupSuggestions(AUTHORITY, MODE);
    }
}
