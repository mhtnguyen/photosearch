package report.bleacher.photosearch.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.SearchRecentSuggestions;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import report.bleacher.photosearch.R;
import report.bleacher.photosearch.adapter.ImageAdapter;
import report.bleacher.photosearch.application.App;
import report.bleacher.photosearch.contentprovider.SuggestionsProvider;
import report.bleacher.photosearch.model.Photo;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static report.bleacher.photosearch.constants.ConstantValues.API_KEY;
import static report.bleacher.photosearch.constants.ConstantValues.EXTRAS;
import static report.bleacher.photosearch.constants.ConstantValues.FORMAT;
import static report.bleacher.photosearch.constants.ConstantValues.METHOD;
import static report.bleacher.photosearch.constants.ConstantValues.NO_JSON_CALLBACK;
import static report.bleacher.photosearch.constants.ConstantValues.PAGE;
import static report.bleacher.photosearch.constants.ConstantValues.PER_PAGE;
import static report.bleacher.photosearch.constants.ConstantValues.TAGS;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        SearchView.OnQueryTextListener {

    private boolean isLoading;
    private boolean isSuggesting;
    private static final String TAG = MainActivity.class.getName();
    private ImageAdapter imageAdapter;
    private GridView gridView;
    private SearchView searchView;
    private int page = 1;
    private int maxPage = Integer.MAX_VALUE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        gridView = (GridView) findViewById(R.id.grid_view);
        imageAdapter = new ImageAdapter(this, new ArrayList<Photo>());
        gridView.setAdapter(imageAdapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                Toast.makeText(MainActivity.this, "" + position,
                        Toast.LENGTH_SHORT).show();
            }
        });
        gridView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                //infinate scroll
                int currentFirstVisPos = view.getFirstVisiblePosition();
                Log.i(TAG, "onScroll: currentPos: " + (visibleItemCount + currentFirstVisPos) + " totalItemCount: " + totalItemCount);
                //prefetch image urls when items are half way
                if (!isLoading && !isSuggesting && (visibleItemCount + currentFirstVisPos > totalItemCount/2) ){
                    page++;
                    makeRequest(MainActivity.this);
                }
            }
        });
        Intent intent = getIntent();
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            SearchRecentSuggestions suggestions = new SearchRecentSuggestions(this,
                    SuggestionsProvider.AUTHORITY, SuggestionsProvider.MODE);
            suggestions.saveRecentQuery(query, null);
            //onQueryTextSubmit still fires search intent when true?? not according to docs
            if (imageAdapter.getCount() == 0) {
                makeRequest(this);
            }
        } else if (Intent.ACTION_VIEW.equals(intent.getAction())) {
            imageAdapter.switchBacktoUnfilteredList();
        } else if (Intent.ACTION_MAIN.equals(intent.getAction())) {
            makeRequest(this);
        }
    }

    private void makeRequest(final Context context) {
        Log.i(TAG, "makeRequest for page: " + page);
        if (page >= maxPage) {
            Snackbar.make(gridView, R.string.max_page, Snackbar.LENGTH_LONG).show();
            return;
        }
        isLoading = true;

        Map<String, String> params = new HashMap<String, String>();
        params.put(API_KEY, "2bc64dda9a84a1ce1a297ee89bc87f1a");
        params.put(FORMAT, "json");
        params.put(NO_JSON_CALLBACK, "1");
        params.put(METHOD, "flickr.photos.search");
        params.put(TAGS, "none");
        params.put(PER_PAGE, "25");
        params.put(PAGE, String.valueOf(page));
        params.put(EXTRAS, "url_s,url_l");

        App.getClient().getPhotos(context, params, new Callback<report.bleacher.photosearch.model.Response>() {
            @Override
            public void onResponse(Call<report.bleacher.photosearch.model.Response> call, Response<report.bleacher.photosearch.model.Response> response) {
                //SingleProgressDialog.getInstance().dismiss();
                isLoading = false;
                if (response.isSuccessful()) {
                    final report.bleacher.photosearch.model.Response rs = response.body();

                    /**
                     * Binding that List to Adapter
                     */
                    if (rs != null && rs.getPhotos() != null) {
                        maxPage = rs.getPhotos().getPages();
                        final List<Photo> photoList = rs.getPhotos().getPhoto();

                        //run on ui
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                //imageAdapter = new ImageAdapter(context, photoList);
                                //gridView.setAdapter(imageAdapter);
                                imageAdapter.addAll(photoList);
                            }
                        });


                    } else {
                        Snackbar.make(gridView, R.string.no_data + " " + response.message(), Snackbar.LENGTH_LONG).show();
                    }

                } else {
                    Snackbar.make(gridView, R.string.some_thing_wrong + " " + response.message(), Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<report.bleacher.photosearch.model.Response> call, Throwable t) {
                //SingleProgressDialog.getInstance().dismiss();
                isLoading = false;
            }
        });
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        SearchManager searchManager = (SearchManager)
                getSystemService(Context.SEARCH_SERVICE);
        MenuItem searchMenuItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) searchMenuItem.getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setSubmitButtonEnabled(true);
        searchView.setOnQueryTextListener(this);
        searchView.setQueryHint("filtering as you type");
        searchView.setOnSuggestionListener(new SearchView.OnSuggestionListener() {

            @Override
            public boolean onSuggestionSelect(int position) {
                return true;
            }

            @Override
            public boolean onSuggestionClick(int position) {
                Cursor cursor = searchView.getSuggestionsAdapter().getCursor();
                cursor.moveToPosition(position);
                String suggestion = cursor.getString(2);//2 is the index of col containing suggestion name.
                getSuggestions(suggestion);
                return true;
            }
        });
        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                if (imageAdapter.getCount() == 0) {

                    imageAdapter.switchBacktoUnfilteredList();
                }
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        //this method calles the search intent and clears photolist unless you return true
        //https://developer.android.com/reference/android/widget/SearchView.OnQueryTextListener.html#onQueryTextSubmit(java.lang.String)
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        getSuggestions(newText);
        return true;
    }

    private void getSuggestions(String newText) {
        isSuggesting = newText == null || newText.isEmpty() ? false : true;
        imageAdapter.getFilter().filter(newText);
    }
}
