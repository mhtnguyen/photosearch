package report.bleacher.photosearch.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import report.bleacher.photosearch.R;

public class DetailsActivity extends AppCompatActivity {
    public static String TITLE_EXTRA="TITLE_EXTRA";
    public static String IMAGE_EXTRA="IMAGE_EXTRA";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.details_layout);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //getSupportActionBar().setHomeAsUpIndicator(R.drawable.white_x);
        String imageUrl = getIntent().getStringExtra(IMAGE_EXTRA);
        String title = getIntent().getStringExtra(TITLE_EXTRA);
        TextView titleTextView = (TextView) findViewById(R.id.title);
        if(imageUrl.isEmpty()){
            title = "No photo result found!!";
        }
        titleTextView.setText(title);
        if(imageUrl!=null && !imageUrl.isEmpty()) {
            ImageView imageView = (ImageView) findViewById(R.id.image);
            Picasso.with(this).load(imageUrl).error(R.drawable.error).into(imageView);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish(); // close this activity

        return false;
    }
}
