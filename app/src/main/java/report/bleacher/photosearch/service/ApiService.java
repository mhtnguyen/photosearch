package report.bleacher.photosearch.service;

import java.util.Map;

import report.bleacher.photosearch.model.Response;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

/**
 * Created by minhhung on 2/24/2018.
 */

public interface ApiService {


    @GET("/services/rest/")
    Call<Response> getPhotos(@QueryMap Map<String, String> params);
}
