package report.bleacher.photosearch.service;

import android.content.Context;
import android.widget.Toast;

import java.util.Map;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import report.bleacher.photosearch.R;
import report.bleacher.photosearch.application.App;
import report.bleacher.photosearch.constants.ConstantValues;
import report.bleacher.photosearch.model.Response;
import report.bleacher.photosearch.utils.InternetConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by minhhung on 2/24/2018.
 */

public class Client {

    /**
     * Get Retrofit Instance
     */

    private final Context context;
    private Retrofit retrofit = null;
    private ApiService apiService = null;
    private static final String TAG = Client.class.getName();


    public Client(Context context) {
        this.context = context;
    }

    private Retrofit getRetrofitInstance() {

        if (retrofit == null) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            httpClient.addInterceptor(logging);

            retrofit = new Retrofit.Builder()
                    .baseUrl(ConstantValues.ROOT_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient.build())
                    .build();
        }

        return retrofit;
    }

    public void getPhotos(final Context context, Map<String, String> params, Callback<Response> callback ) {
       if (InternetConnection.checkConnection(context)) {

            //SingleProgressDialog.getInstance().show(context,context.getString(R.string.please_wait),"Getting photos");
            //Creating an object of our api interface
            ApiService api = App.getClient().getApiService();

            Call<Response> call = api.getPhotos(params);

            call.enqueue(callback);

        } else {
            Toast.makeText(context, R.string.internet_connection_not_available,
                    Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Get API Service
     *
     * @return API Service
     */
    private ApiService getApiService() {
        if(apiService==null) {
            apiService = getRetrofitInstance().create(ApiService.class);
        }
        return apiService;
    }
}
