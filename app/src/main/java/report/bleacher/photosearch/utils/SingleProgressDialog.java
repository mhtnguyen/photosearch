package report.bleacher.photosearch.utils;

import android.app.ProgressDialog;
import android.content.Context;

import report.bleacher.photosearch.R;

/**
 * Created by minhhung on 2/26/2018.
 */

public class SingleProgressDialog {
    private ProgressDialog dialog;
    private static SingleProgressDialog mInstance;

    public static synchronized SingleProgressDialog getInstance() {
        if (mInstance == null) {
            mInstance = new SingleProgressDialog();
        }
        return mInstance;
    }

    public void show(Context context,String title, String message) {
        if (dialog != null && dialog.isShowing()) {
            return;
        }
        dialog = new ProgressDialog(context,R.style.ProgressDialog);
        dialog.setTitle(title);
        dialog.setMessage(message);
        dialog.show();
    }

    public void dismiss() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }
}
