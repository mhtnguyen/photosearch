package report.bleacher.photosearch.constants;

/**
 * Created by minhhung on 2/24/2018.
 */

public class ConstantValues {
    /********
     * URLS PARAMS
     *******/
    public static final String API_KEY = "api_key";
    public static final String FORMAT = "format";
    public static final String NO_JSON_CALLBACK = "nojsoncallback";
    public static final String METHOD = "method";
    public static final String TAGS = "tags";
    public static final String PER_PAGE = "per_page";
    public static final String PAGE = "page";
    public static final String EXTRAS = "extras";

    /********
     * URLS
     *******/
    public static final String ROOT_URL = "https://api.flickr.com/";


}
