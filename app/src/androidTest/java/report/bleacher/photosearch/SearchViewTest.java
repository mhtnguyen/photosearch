package report.bleacher.photosearch;

/**
 * Created by minhhung on 2/27/2018.
 */

import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.LargeTest;

import report.bleacher.photosearch.activities.MainActivity;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Demonstrates Espresso with action bar and app compat searchview widget
 */
@LargeTest
public class SearchViewTest extends
        ActivityInstrumentationTestCase2<MainActivity> {
    @SuppressWarnings("deprecation")
    public SearchViewTest() {
        super("report.bleacher.photosearch", MainActivity.class);
    }
    @Override
    public void setUp() throws Exception {
        super.setUp();
        getActivity();
    }
    @SuppressWarnings("unchecked")
    public void testAppCompatSearchViewFromActionBar() {
        onView(withId(R.id.action_search))
                .perform(click());

        onView(withId(R.id.search_src_text))
                .perform(typeText("Hello World")).check(matches(withText("Hello World")));
    }
}
